# Notification Plugin for Ubuntu/Ubports devices

This plugin consists of

* dekkod-notify - service that watches dekkod for new mail and posts notifications
* dekko-notification-plugin - service plugin that manages dekkod-notify
* dekko-notification-settings - settings plugin to configure notifications

